
console.log('1');
/*
Bài 1: Tính tiền lương nhân viên
Viết chương trình tính tiền lương nhân viên trình
Lương 1 ngày:100.000
Cho người dùng nhập vào số ngày làm 
Công thức tính lương:Lương 1 nngày * số ngày làm
*/

/*
INPUT
- User Nhập vào số ngày làm việc 

PROCESS
- tạo hàm fuction tính tiền lương nv
- tạo biến số ngày làm việc và tìm đến thẻ chứa số ngày làm việc mà user nhập vào 
sao đó lấy giá trị gán vào biến số ngày làm việc mới tạo
- tạo 1 biến const có giá trị không thể khai báo lại là tiền lương 1 ngày công
- tạo 1 biến chứa kết quả tính toán số ngày làm * lương 1 ngày công 
- gán biến chứa kết quả cho nội dung thẻ muốn thể hiện ra màn hình
- gán sự kiện click cho button
OUTPUT
- Kết quả của hàm tính lương
*/

function tinhLuongNV() {
    var numberDayWork = document.getElementById('solary').value;
    const moneyOfDay = 100000;
    var total = numberDayWork * moneyOfDay;
    document.getElementById('luong').innerHTML = total.toLocaleString();
}


/*
Bài 2: Tính giá trung Bình
Viết chươgn trình nhập vào 5 số thực.
Tính giá trị trung bình của 5 số này và xuất ra màn hình
*/

/*
INPUT
- User nhập vào các giá trị cần thiết ở các ô input
PROCESS
- tạo các biến chứa giá trị mà user nhập vào 
- ép kiểu các biến chứa giá trị người dùng nhập vào thành kiểu INT
- tạo biến chứa kế quả tính toán
- gán biến chứa kết quả cho nội dung thẻ muốn thể hiện ra màn hình
- gán sự kiện click cho button
OUTPUT
- kết quả của tổng 5 số người dung nhập vào
*/
function total5Number() {
    var number1 = document.getElementById('input_1').value;
    var number2 = document.getElementById('input_2').value;
    var number3 = document.getElementById('input_3').value;
    var number4 = document.getElementById('input_4').value;
    var number5 = document.getElementById('input_5').value;
    var total = parseInt(number1) + parseInt(number2) + parseInt(number3) + parseInt(number4) + parseInt(number5)
    document.getElementById('total__5__number').innerHTML = total;
}


/*
Bài 3 Quy Đổi Tiền
Giá USD hiện này là 23.500 VNĐ 
Viết chươgn trình quy đổi USD sang VND.
Cho người dùng nhập vào số tiền USD.
tính và xuât ra số tiền sau quy đổi VND
vd: nhập 2 USD => xuất ra 47.000
*/

/*
INPUT
- User nhập vào số tiền USD muốn quy đổi sang VNĐ 
PROCESS
- Tạo biến chứa giá trị số tiền USD mà người dùng nhập vào
- tạo biến const chứa giá trị của 1 đô là 23000 vnđ
- ép kiểu của biến chứa usd người dung nhập vào c
- tạo biến chứa kế quả tính toán
- gán biến chứa kết quả cho nội dung thẻ muốn thể hiện ra màn hình
- gán sự kiện click cho button
OUTPUT
- Số tiền VNĐ đã được quy đổi từ USĐ
*/
function changeMoney() {
    var moneyUSD = document.getElementById('USD').value;
    const VND = 23500;
    var total = parseInt(moneyUSD) * VND;
    document.getElementById('VND').innerHTML = total.toLocaleString();
}


/*
Bài 4 Tính diện tích, chu vi hình chữ Nhật
Viết chươgn trình nhập vào 2 chiều dài và chiều rộng
HCN
Tính và xuất ra diện tích, chu vi của HCN đó
CT:
- Diện tích = dài * rộng
- Chu vi = ( Dài + rộng )*2
*/

/*
INPUT
- user nhập vào chiều dài, chiều rộng
PROCESS
- tạo 2 biến chứa giá trị chiều dài và chiều rộng mà user nhập vào
- tạo biến diện tích chứa kết qủa tính toán theo công thức tính diện tích  dài * rộng
- tạo biến chu vi chứa kết quả tính toán theo công thức tính chu vi (dài + rộng)*2
- gán biến chứa kết quả cho nội dung thẻ muốn thể hiện ra màn hình
- gán sự kiện click cho button
OUTPUT
- kết quả của diện tích , chu vi hình chữ nhật

*/

function HCN() {
    var widthHCN = document.getElementById('width').value;
    var heightHCN = document.getElementById('height').value;
    var acreage = widthHCN * heightHCN;
    var perimeter = (heightHCN + widthHCN) * 2;
    document.getElementById('DT').innerHTML = acreage;
    document.getElementById('CV').innerHTML = perimeter;
}


/*
Bài 5: tính tổng 2 ký số
Viết chương trình nhập vào 2 chữ số (VD: 12, 44, 83)
Tính tổng 2 ký số của số vừa nhập
Ví dụ: 
12 => Tổng 1 + 2 = 3

Lây số hàng đơn vị: int so_hang_dv = so % 10;
lấy số hàng chục: int so_hang_chuc = so/10;
*/

/*
INPUT
- user nhập vào số có 2 chữ số 
PROCESS
- tạo biến chưa giá trị mà user nhập vào
- tạo biến chứa kí tự đầu tiên của của số user nhập vào bằng cách chia lấy phần nguyên
- tạo biến chứa kí tự thứ hai của số user nhập vào bằng cách chia lấy phần dư
- tạo biến chưa kết quả tổng của 2 biến chứa 2 kí tự cần tính
- gán biến chứa kết quả cho nội dung thẻ muốn thể hiện ra màn hình
- gán sự kiện click cho button
OUTPUT
- kết quả tổng ký số mà user nhập vào 
*/
function two_character_total() {
    var numberInput = document.getElementById('input-number').value;
    var second_number = numberInput % 10;
    var first_number = Math.floor(numberInput / 10);
    var total = first_number + second_number;
    document.getElementById('tong').innerHTML = total;
}


// Button
const BT_1 = document.querySelector('.BT_1');
const BT_1_Active = document.getElementById('BT-1');
const BT_2 = document.querySelector('.BT_2');
const BT_2_Active = document.getElementById('BT-2');
const BT_3 = document.querySelector('.BT_3');
const BT_3_Active = document.getElementById('BT-3');
const BT_4 = document.querySelector('.BT_4');
const BT_4_Active = document.getElementById('BT-4');
const BT_5 = document.querySelector('.BT_5');
const BT_5_Active = document.getElementById('BT-5');

BT_1.addEventListener('click', () => {
    BT_1_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
    BT_5_Active.classList.remove('active')
})

BT_2.addEventListener('click', () => {
    BT_2_Active.classList.toggle('active')
    BT_1_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
    BT_5_Active.classList.remove('active')
})

BT_3.addEventListener('click', () => {
    BT_3_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_1_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
    BT_5_Active.classList.remove('active')
})

BT_4.addEventListener('click', () => {
    BT_4_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_1_Active.classList.remove('active')
    BT_5_Active.classList.remove('active')
})

BT_5.addEventListener('click', () => {
    BT_5_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
    BT_1_Active.classList.remove('active')
})